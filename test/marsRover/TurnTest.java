package marsRover;

import org.junit.Assert;
import org.junit.Test;

import static marsRover.Turn.*;

public class TurnTest {

  @Test
  public void turnLeft(){
    Assert.assertEquals(-90, LEFT.getDeviationAngle());
  }

  @Test
  public void turnRight(){
    Assert.assertEquals(90, RIGHT.getDeviationAngle());
  }
}
