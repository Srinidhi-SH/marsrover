package marsRover;

import org.junit.Assert;
import org.junit.Test;

public class RoverTest {

  @Test
  public void moveARover(){
    Rover rover = rover();
    rover.move();
    Assert.assertEquals("1 2 N",rover.roverIsIn());
  }

  @Test
  public void turnRoverLeft(){
    Rover rover = rover();
    rover.turnLeft();
    Assert.assertEquals("1 1 W",rover.roverIsIn());
  }

  @Test
  public void turnRoverRight(){
    Rover rover = rover();
    rover.turnRight();
    Assert.assertEquals("1 1 E",rover.roverIsIn());
  }

  private Rover rover(){
    return new Rover(new Location(1,1,Direction.N));
  }
}
