package marsRover;

import org.junit.Assert;
import org.junit.Test;

import static marsRover.Direction.*;
public class RoverControlTest {

  @Test
  public void executeInstructionsOnRover(){
    RoverControl roverControl = new RoverControl(5,5);
    roverControl.landTheRover(new Rover(new Location(1,2,N)));
    Assert.assertEquals("1 3 N",roverControl.executeInstructions("LMLMLMLMM"));
  }

  @Test
  public void executeAnotherSetOfInstructionsOnRover(){
    RoverControl roverControl = new RoverControl(5,5);
    roverControl.landTheRover(new Rover(new Location(3,3,E)));
    Assert.assertEquals("5 1 E",roverControl.executeInstructions("MMRMMRMRRM"));
  }

  @Test
  public void roverOutsideThePlateau(){
    RoverControl roverControl = new RoverControl(5,5);
    roverControl.landTheRover(new Rover(new Location(4,4,E)));
    Assert.assertEquals("Rover Outside Plateau",roverControl.executeInstructions("MMRMMRMRRM"));
  }
}
