package marsRover;

import org.junit.Assert;
import org.junit.Test;

import static marsRover.Direction.*;
import static marsRover.Turn.*;

public class LocationTest {

  @Test
  public void moveNorthAndChangeLocation(){
    Assert.assertEquals("1 2 N",new Location(1,1,N).move().toString());
  }

  @Test
  public void moveSouthAndChangeLocation(){
    Assert.assertEquals("1 0 S",new Location(1,1,S).move().toString());
  }

  @Test
  public void moveEastAndChangeLocation(){
    Assert.assertEquals("2 1 E",new Location(1,1,E).move().toString());
  }

  @Test
  public void moveWestAndChangeLocation(){
    Assert.assertEquals("0 1 W",new Location(1,1,W).move().toString());
  }

  @Test
  public void turnLeftFromNorth(){
    Assert.assertEquals("1 1 W",new Location(1,1,N).turn(LEFT).toString());
  }
}
