package marsRover;

import org.junit.Assert;
import org.junit.Test;

import static marsRover.Direction.*;
import static marsRover.Turn.*;

public class DirectionTest {

  @Test
  public void turnLeftFromNorth(){
    Assert.assertEquals(W,N.changeDirection(LEFT));
  }

  @Test
  public void turnRightFromNorth(){
    Assert.assertEquals(E,N.changeDirection(RIGHT));
  }

  @Test
  public void turnLeftFromSouth(){
    Assert.assertEquals(E,S.changeDirection(LEFT));
  }

  @Test
  public void turnRightFromSouth(){
    Assert.assertEquals(W,S.changeDirection(RIGHT));
  }
}
