package Driver;

import marsRover.Direction;
import marsRover.Location;
import marsRover.Rover;
import marsRover.RoverControl;

import java.util.Scanner;

import static java.lang.Integer.*;

public class MarsRoverDriver {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String rightTop[] = scanner.nextLine().split(",");
    RoverControl roverControl = new RoverControl(parseInt(rightTop[0]),parseInt(rightTop[1]));
    String rover[] = scanner.nextLine().split(" ");
    String instructions = scanner.nextLine();
    roverControl.landTheRover(getRover(rover));
    System.out.println(roverControl.executeInstructions(instructions));
  }

  private static Rover getRover(String directions[]) {
    return new Rover(new Location(parseInt(directions[0]),parseInt(directions[1]), Direction.valueOf(directions[2])));
  }
}
