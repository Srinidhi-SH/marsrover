package marsRover;

/**
 * Represents all details on, where is the Rover
 */
public class Location {

  private final int xCoordinate;
  private final int yCoordinate;
  private final Direction direction;

  public Location(int xCoordinate, int yCoordinate, Direction direction) {
    this.xCoordinate = xCoordinate;
    this.yCoordinate = yCoordinate;
    this.direction = direction;
  }

  protected Location turn(Turn turn) {
    return new Location(xCoordinate, yCoordinate, direction.changeDirection(turn));
  }

  protected Location move() {
    return new Location(this.xCoordinate + direction.getXIncrementFactor(), this.yCoordinate + direction.getYIncrementFactor(), direction);
  }

  protected boolean isInsideDimensions(int maxX, int maxY) {
    if (xCoordinate >= 0 && xCoordinate <= maxX && xCoordinate >= 0 && yCoordinate <= maxY) return true;
    return false;
  }

  @Override
  public String toString() {
    return xCoordinate + " " + yCoordinate + " " + direction;
  }
}
