package marsRover;

/**
 * All operations rover is capable of doing
 */
public interface RoverOperations {
  Move M = new Move();
  TurnLeft L = new TurnLeft();
  TurnRight R = new TurnRight();
  void execute(Rover rover);
}

class Move implements RoverOperations{
  @Override
  public void execute(Rover rover) {
    rover.move();
  }
}

class TurnLeft implements  RoverOperations{
  @Override
  public void execute(Rover rover) {
    rover.turnLeft();
  }
}

class TurnRight implements  RoverOperations{
  @Override
  public void execute(Rover rover) {
    rover.turnRight();
  }
}
