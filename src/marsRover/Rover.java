package marsRover;

import static marsRover.Turn.*;

/**
 * A machine that captures Images and moves based on instructions
 */
public class Rover {

  private Location locationOfRover ;
  public Rover(Location locationOfRover){
    this.locationOfRover=locationOfRover;
  }

  protected void move(){
    locationOfRover = locationOfRover.move();
  }

  protected void turnLeft(){
    locationOfRover = locationOfRover.turn(LEFT);
  }

  protected void turnRight(){
    locationOfRover = locationOfRover.turn(RIGHT);
  }

  protected String roverIsIn(){
    return locationOfRover.toString();
  }

  protected boolean isInsideThePlateau(int maxX,int maxY) {
    return locationOfRover.isInsideDimensions(maxX,maxY);
  }
}
