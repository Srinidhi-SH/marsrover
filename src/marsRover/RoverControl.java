package marsRover;


import java.util.LinkedHashMap;

import static marsRover.RoverOperations.*;

/**
 * Based on the instructions executes instructions on the rover
 */
public class RoverControl {
  private Rover roverInPlateau ;
  private LinkedHashMap operations = new LinkedHashMap();
  private int maxXCoOrdinate,maxYCoOrdinate;

  public RoverControl(int maxXCoOrdinate,int maxYCoOrdinate){
    this.maxXCoOrdinate = maxXCoOrdinate;
    this.maxYCoOrdinate = maxYCoOrdinate;
    operations.put('M', M);
    operations.put('L', L);
    operations.put('R', R);
  }

  public void landTheRover(Rover rover){

    this.roverInPlateau = rover;
  }

  public String executeInstructions(String instructions){
    int length = instructions.length();
    for (int index = 0; index< length; index++){
      RoverOperations operation = (RoverOperations)operations.get(instructions.charAt(index));
      operation.execute(roverInPlateau);
    }
    if (roverInPlateau.isInsideThePlateau(maxYCoOrdinate,maxYCoOrdinate))return roverInPlateau.roverIsIn();
  return "Rover Outside Plateau";
  }
}
