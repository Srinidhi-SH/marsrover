package marsRover;

/**
 * To make a turn angle needed to turn
 */
public enum Turn {
  LEFT(-90), RIGHT(90);

  private int deviationAngle;

  Turn(int deviationAngle) {

    this.deviationAngle = deviationAngle;
  }
  public int getDeviationAngle(){
    return deviationAngle;
  }
}
