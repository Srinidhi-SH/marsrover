package marsRover;

/**
 * Angle associated with direction and incremental steps to move in that direction
 */
public enum Direction {
  N(0, 0, 1), S(180, 0, -1), E(90, 1, 0), W(270, -1, 0);

  private int angle;
  private int xIncrementFactor;
  private int yIncrementFactor;

  Direction(int angle, int xIncrementFactor, int yIncrementFactor) {
    this.angle = angle;
    this.xIncrementFactor = xIncrementFactor;
    this.yIncrementFactor = yIncrementFactor;
  }

  public int getXIncrementFactor() {
    return xIncrementFactor;
  }

  public int getYIncrementFactor() {
    return yIncrementFactor;
  }

  public Direction changeDirection(Turn turn) {
    int changedAngle = (turn.getDeviationAngle() + angle) % 360;
    if (changedAngle < 0) changedAngle += 360;
    for (Direction direction : Direction.values())
      if (direction.angle == changedAngle) return direction;
    return null;
  }
}
